package com.example.simplenetworkingmovieapps.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.simplenetworkingmovieapps.api.RemoteDataSource
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MovieRepositoryTest {

    private lateinit var movieRepository: MovieRepository

    @MockK
    private lateinit var remoteDataSource: RemoteDataSource

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
        movieRepository = MovieRepository(remoteDataSource)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun searchMovie() {

    }

    @Test
    fun moviePopular() {
        //Apakah movieRepository moviePopular not null?
        runBlocking {
            assertNotNull(movieRepository.searchMovie("Agak Laen"))
        }

    }
}