package com.example.simplenetworkingmovieapps.api

import com.example.simplenetworkingmovieapps.api.ApiService

class RemoteDataSource(private val apiService: ApiService) {

    suspend fun searchMovie(query: String) = apiService.searchMovie(query = query)
    suspend fun moviePopular() = apiService.getMoviePopular()
}