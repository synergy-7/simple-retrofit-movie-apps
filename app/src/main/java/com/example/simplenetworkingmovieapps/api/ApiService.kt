package com.example.simplenetworkingmovieapps.api

import com.example.simplenetworkingmovieapps.ApiKey
import com.example.simplenetworkingmovieapps.api.response.MovieResponse
import com.example.simplenetworkingmovieapps.api.request.RatingRequest
import com.example.simplenetworkingmovieapps.api.response.RatingResponse
import com.example.simplenetworkingmovieapps.api.response.Result
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/now_playing")
    fun getMovieNowPlaying(
        //@Header("Authorization") apikey: String = ApiKey.key
    ): Call<MovieResponse>

    @GET("movie/popular")
    suspend fun getMoviePopular(
        //@Header("Authorization") apikey: String = ApiKey.key
    ): MovieResponse

    @GET("movie/{movie_id}")
    fun getMovieDetail(
        //@Header("Authorization") apikey: String = ApiKey.key,
        @Path("movie_id") movieId: String
    ): Call<Result>

    @POST("movie/{movie_id}/rating")
    fun addRating(
        //@Header("Authorization") apikey: String = ApiKey.key,
        @Path("movie_id") movieId: String,
        @Body request: RatingRequest
    ): Call<RatingResponse>

    @GET("search/movie")
    suspend fun searchMovie(
        //@Header("Authorization") apikey: String = ApiKey.key,
        @Query("query") query: String
    ): MovieResponse
}