package com.example.simplenetworkingmovieapps.api

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.example.simplenetworkingmovieapps.ApiKey
import com.example.simplenetworkingmovieapps.App
import com.example.simplenetworkingmovieapps.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private const val BASE_URL = BuildConfig.BASE_URL

    private val logging: HttpLoggingInterceptor
        get() {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            return httpLoggingInterceptor.apply {
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            }
        }
    private val client = OkHttpClient.Builder()
        .addInterceptor(logging)
        .addInterceptor(ChuckerInterceptor(App.context!!))
        .addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", ApiKey.key)
                .build()
            chain.proceed(request)
        }
        .build()

    val instance: ApiService by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        retrofit.create(ApiService::class.java)
    }

}