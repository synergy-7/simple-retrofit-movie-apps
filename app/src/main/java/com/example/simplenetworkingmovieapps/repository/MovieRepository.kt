package com.example.simplenetworkingmovieapps.repository

import com.example.simplenetworkingmovieapps.api.RemoteDataSource
import com.example.simplenetworkingmovieapps.api.response.toMovie
import com.example.simplenetworkingmovieapps.model.Movie

/*
* Tugasnya
*   Memilih data yang dipakai itu sumber dari mana?
*   Apa dari remote data source?
*   Apa dari local data source?
* */
class MovieRepository(private val remoteDataSource: RemoteDataSource) {

    suspend fun searchMovie(query: String) = remoteDataSource.searchMovie(query)
    suspend fun moviePopular(): List<Movie?>? = remoteDataSource.moviePopular().results?.map { it?.toMovie() }
}