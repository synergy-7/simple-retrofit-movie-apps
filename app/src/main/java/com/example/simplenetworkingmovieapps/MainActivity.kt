package com.example.simplenetworkingmovieapps

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import com.example.simplenetworkingmovieapps.api.ApiClient
import com.example.simplenetworkingmovieapps.databinding.ActivityMainBinding
import com.example.simplenetworkingmovieapps.helper.Status
import com.example.simplenetworkingmovieapps.api.request.RatingRequest
import com.example.simplenetworkingmovieapps.api.response.MovieResponse
import com.example.simplenetworkingmovieapps.api.response.RatingResponse
import com.example.simplenetworkingmovieapps.api.response.Result
import com.example.simplenetworkingmovieapps.viewmodel.MovieViewModel
import com.example.simplenetworkingmovieapps.viewmodel.MovieViewModelFactory
import com.google.gson.Gson
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    /*private val viewModel: MovieViewModel by viewModels {
        MovieViewModelFactory.getInstance(this)
    }*/
    private val viewModel: MovieViewModel by inject()
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ApiClient.instance.getMovieNowPlaying().enqueue(object : Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                Log.e("SimpleDataAPI", Gson().toJson(response.body()))
            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Log.e("SimpleDataAPI", Gson().toJson(t.message))
            }

        })

        ApiClient.instance.getMovieDetail(movieId = "653346").enqueue(object : Callback<Result> {
            override fun onResponse(call: Call<Result>, response: Response<Result>) {
                Log.e("SimpleDataAPIDetail", Gson().toJson(response.body()))
            }

            override fun onFailure(call: Call<Result>, t: Throwable) {
                Log.e("SimpleDataAPIDetail", Gson().toJson(t.message))
            }
        })

        val request = RatingRequest(8.5)
        ApiClient.instance.addRating(movieId = "653346", request = request)
            .enqueue(object : Callback<RatingResponse> {
                override fun onResponse(
                    call: Call<RatingResponse>,
                    response: Response<RatingResponse>
                ) {
                    Log.e("SimpleDataAPIRating", Gson().toJson(response.body()))
                }

                override fun onFailure(call: Call<RatingResponse>, t: Throwable) {
                    Log.e("SimpleDataAPIRating", Gson().toJson(t.message))
                }
            })



        viewModel.getMoviePopular().observe(this){
            Log.e("SimpleDomain", Gson().toJson(it))
        }

        viewModel.getMovieNowPlaying()
        viewModel.movieResponse.observe(this){

        }

        viewModel.getIncrement().observe(this){
            Log.e("SimpleDataStore", it.toString())
            binding.text.text = it.toString()
        }

    }
    fun trigger(v: View){
        viewModel.searchMovie("Agak Laen").observe(this) { resources ->
            when (resources.status) {
                Status.SUCCESS -> {
                    val data = resources.data
                    Log.e("SimpleRetrofit", Gson().toJson(data))
                }

                Status.ERROR -> {
                    val response = resources.message
                    Log.e("SimpleRetrofit", response.toString())
                }

                Status.LOADING -> {}
            }
        }    }
}