package com.example.simplenetworkingmovieapps.usecase

import com.example.simplenetworkingmovieapps.repository.MovieRepository

class MovieUseCase(val repository: MovieRepository) {
    suspend fun getData(){
        repository.moviePopular()
    }
}