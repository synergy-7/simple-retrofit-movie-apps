package com.example.simplenetworkingmovieapps

import android.app.Application
import com.example.simplenetworkingmovieapps.api.ApiClient
import com.example.simplenetworkingmovieapps.api.RemoteDataSource
import com.example.simplenetworkingmovieapps.helper.MyDataStore
import com.example.simplenetworkingmovieapps.repository.MovieRepository
import com.example.simplenetworkingmovieapps.viewmodel.MovieViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object KoinModule {
    /*
    * untuk menginstansiasi sebuah object
    * menggunakan
    * -  single atau
    * -  factory
    *
    * untuk menginstansiasi sebuah viewmodel
    * menggunakan viewmodel
    *
    * fungsi get() bertujuan untuk memanggil yang object yang di instansiasi.
    * */
    val Application.dataModule
        get() = module {
            //DATABASE
            single { MyDataStore(get()) }
            //API
            single { ApiClient.instance }
            //REPOSITORY
            factory { RemoteDataSource(get()) }
            //REPOSITORY
            factory { MovieRepository(get()) }

        }

    val Application.uiModule
        get() = module {
            viewModel { MovieViewModel(get(), get()) }
        }
}