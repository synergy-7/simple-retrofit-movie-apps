package com.example.simplenetworkingmovieapps.helper

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class MyDataStore (val context: Context) {
    val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "counter")
    private val COUNTER_KEY = intPreferencesKey("counter_key") //1
    private val IS_LOGIN_KEY = booleanPreferencesKey("is_login_key") //false

    suspend fun incrementCounter() {
        context.dataStore.edit { preferences ->
            val currentCounterValue = preferences[COUNTER_KEY] ?: 0
            preferences[COUNTER_KEY] = currentCounterValue + 1
        }
    }

    suspend fun setLogin(_isLogin: Boolean){
        context.dataStore.edit { preferences ->
            preferences[IS_LOGIN_KEY] = _isLogin
        }
    }

    suspend fun getPreferences(context: Context): Flow<Preferences> {
        return context.dataStore.data
    }

    fun getCounter(): Flow<Int> {
        return context.dataStore.data.map { preferences ->
            preferences[COUNTER_KEY] ?: 0
        }
    }

    fun getLogin(): Flow<Boolean> {
        return context.dataStore.data.map {preferences ->
            preferences[IS_LOGIN_KEY] ?: false
        }
    }


}