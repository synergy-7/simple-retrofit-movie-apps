package com.example.simplenetworkingmovieapps.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.simplenetworkingmovieapps.api.ApiClient
import com.example.simplenetworkingmovieapps.api.response.MovieResponse
import com.example.simplenetworkingmovieapps.helper.MyDataStore
import com.example.simplenetworkingmovieapps.helper.Resource
import com.example.simplenetworkingmovieapps.repository.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieViewModel(private val repository: MovieRepository, private var pref: MyDataStore) : ViewModel() {

    private var _movieResponse = MutableLiveData<MovieResponse?>()
    val movieResponse: LiveData<MovieResponse?> get() = _movieResponse

    //async
    fun searchMovie(query: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(data = repository.searchMovie(query)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun getMoviePopular() = liveData(Dispatchers.IO) {
        emit(repository.moviePopular())
    }

    fun getMovieNowPlaying() {
        ApiClient.instance.getMovieNowPlaying().enqueue(object : Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                val data = response.body()
                _movieResponse.postValue(data)
            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                _movieResponse.postValue(null)
            }

        })
    }

    fun setIncrement(){
        viewModelScope.launch {
            pref.incrementCounter()
        }
    }

    fun getIncrement() = pref.getCounter().asLiveData()

    fun setLogin(isLogin: Boolean){
        viewModelScope.launch {
            pref.setLogin(isLogin)
        }
    }

    fun getLogin() = pref.getLogin().asLiveData()
}

