package com.example.simplenetworkingmovieapps.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.simplenetworkingmovieapps.api.ApiClient
import com.example.simplenetworkingmovieapps.api.RemoteDataSource
import com.example.simplenetworkingmovieapps.helper.MyDataStore
import com.example.simplenetworkingmovieapps.repository.MovieRepository

class MovieViewModelFactory(val remoteDataSource: RemoteDataSource, val pref: MyDataStore) :
    ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
        private var instance: MovieViewModelFactory? = null

        fun getInstance(context: Context): MovieViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: MovieViewModelFactory(
                    RemoteDataSource(ApiClient.instance),
                    MyDataStore(context)
                )
            }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieViewModel::class.java)) {
            return MovieViewModel(MovieRepository(remoteDataSource), pref) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}