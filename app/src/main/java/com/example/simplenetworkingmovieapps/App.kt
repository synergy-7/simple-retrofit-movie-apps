package com.example.simplenetworkingmovieapps

import android.app.Application
import android.content.Context
import com.example.simplenetworkingmovieapps.KoinModule.dataModule
import com.example.simplenetworkingmovieapps.KoinModule.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    dataModule, uiModule
                )
            )
        }
    }

    companion object {
        var context: Application? = null
    }

    /*
    * add koin module at gradle
    * create module
    * startkoin dan panggil module di App
    * panggil App di manifest
    * */
}