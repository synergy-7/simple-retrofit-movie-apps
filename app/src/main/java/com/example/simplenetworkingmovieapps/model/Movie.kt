package com.example.simplenetworkingmovieapps.model

data class Movie (
    val id: Int?,
    val originalTitle: String?,
    val overview: String?,
    val posterPath: String?
)